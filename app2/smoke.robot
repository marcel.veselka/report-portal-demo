*** Settings ***
Resource  ../resources/flakiness.resource
Force Tags  feature:smoke

*** Test Cases ***
smoke: DB is available
    [Tags]  lvl:smoke  lvl:mon  component:db
    Flakiness  2  sys_bug  Smoke test to check db is available.

smoke: Application is up
    [Tags]  lvl:smoke  lvl:mon  app:app2
    Flakiness  5  sys_bug  Smoke test to check application is available.

smoke: Login works
    [Tags]  lvl:smoke  app:app2
    Flakiness  0  prod_bug  Unable to login into application.

smoke: Admin login works
    [Tags]  lvl:smoke  app:app2
    Flakiness  0  prod_bug  Unable to login into application as admin