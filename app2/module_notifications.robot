*** Settings ***
Resource  ../resources/flakiness.resource
Force Tags    feature:notifications

*** Test Cases ***
resgression: Notify - simple flow
    [Tags]  lvl:regress
    Flakiness  2  prod_bug  Regression test to check whether simple flow works.

resgression: Notify - alternative flow 1
    [Tags]  lvl:regress
    Flakiness  0  prod_bug  Regression test to check whether alternative flow works.

resgression: Notify - negative flow 1
    [Tags]  lvl:regress
    Flakiness  5  prod_bug  Regression test to check whether negative flow works.

resgression: Notify - negative flow 2
    [Tags]  lvl:regress
    Flakiness  5  prod_bug  Regression test to check whether negative flow works.

resgression: Notify - negative flow 3
    [Tags]  lvl:regress
    Flakiness  5  prod_bug  Regression test to check whether negative flow works.

resgression: Notify - negative flow 4
    [Tags]  lvl:regress
    Flakiness  5  prod_bug  Regression test to check whether negative flow works.

resgression: Notify - negative flow 5, never fails
    [Tags]  lvl:regress
    Flakiness  0  prod_bug  Regression test to check whether negative flow works.