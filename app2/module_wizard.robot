*** Settings ***
Resource  ../resources/flakiness.resource
Force Tags    feature:wizard

*** Test Cases ***
resgression: Wizard - simple flow
    [Tags]  lvl:regress
    Flakiness  2  prod_bug  Regression test to check whether simple flow works.

resgression: Wizard - alternative flow 1
    [Tags]  lvl:regress
    Flakiness  0  prod_bug  Regression test to check whether alternative flow works.

resgression: Wizard - alternative flow 2
    [Tags]  lvl:regress
    Flakiness  10  prod_bug  Regression test to check whether alternative flow works.

resgression: Wizard - alternative flow 3
    [Tags]  lvl:regress
    Flakiness  30  prod_bug  Regression test to check whether alternative flow works.

resgression: Wizard - complex flow
    [Tags]  lvl:regress
    Flakiness  30  prod_bug  Regression test to check whether complex flow works.

resgression: Wizard - negative flow 1
    [Tags]  lvl:regress
    Flakiness  5  prod_bug  Regression test to check whether negative flow works.

resgression: Wizard - negative flow 2
    [Tags]  lvl:regress
    Flakiness  5  prod_bug  Regression test to check whether negative flow works.

resgression: Wizard - negative flow 3
    [Tags]  lvl:regress
    Flakiness  5  prod_bug  Regression test to check whether negative flow works.

resgression: Wizard - negative flow 4
    [Tags]  lvl:regress
    Flakiness  5  prod_bug  Regression test to check whether negative flow works.

resgression: Wizard - negative flow 5, always fails
    [Tags]  lvl:regress
    Flakiness  100  prod_bug  Regression test to check whether negative flow works.