*** Settings ***
Resource         ../resources/flakiness.resource
Library          ../resources/attach.py
Library          SeleniumLibrary
Test Teardown    Close Browser
Test Setup       Start test
Test Template    Login
Force Tags       feature:login

*** Variables ***
${URL}        not-here-see-variables-py
${BROWSER}    headlesschrome               # Chrome    # Firefox

${H1}    Demo App

${VALID USER}        marcel.veselka@tesena.com
${VALID PASSWORD}    asdf
#${EMPTY}

*** Test Cases ***                USERNAME         PASSWORD             EXPECTED
Valid User Name                   ${VALID USER}    ${VALID PASSWORD}    True
Invalid User Name                 invalid          ${VALID PASSWORD}    False
Invalid Password                  ${VALID USER}    invalid              True        # This should pass until password chech is not implemented
Invalid User Name and Password    invalid          invalid              False
Empty User Name                   ${EMPTY}         ${VALID PASSWORD}    False
Empty Password                    ${VALID USER}    ${EMPTY}             False
Empty User Name and Password      ${EMPTY}         ${EMPTY}             False

*** Keywords ***
Start test
    Open Browser                ${URL}    ${BROWSER}    options=add_argument("--ignore-certificate-errors")
    Wait Until Page Contains    ${H1}

Login
    [arguments]       ${user}=asdf    ${psswd}=marcel.veselka@tesena.com    ${expected}='PASS'
    Input Text        name=user       ${user}
    Input Password    name=pswd       ${psswd}
    Click Element     name=login

    ${path}        Capture Page Screenshot
    log screens    ${path}

    ${status}=        Run Keyword And Return Status    Wait Until Element Is Visible    id:logedin
    Should Be True    '${status}'=='${expected}'
