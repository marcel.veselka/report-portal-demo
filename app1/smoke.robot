*** Settings ***
Resource  ../resources/flakiness.resource
Force Tags  feature:smoke

*** Test Cases ***
smoke test: DB is available
    [Tags]  lvl:smoke  component:db
    Flakiness  20  sys_bug  Smoke test to check db is available.

smoke test: Application is up
    [Tags]  lvl:mon  lvl:smoke  app:app1
    Flakiness  5  prod_bug  Smoke test to check application is available.

smoke test: Login works
    [Tags]  lvl:smoke  app:app1
    Flakiness  5  no_defect  Unable to login into application.