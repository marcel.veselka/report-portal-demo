*** Settings ***
Resource  ../resources/flakiness.resource
Force Tags    feature:contract closure


*** Test Cases ***
regression: Contract closure - simple flow
    [Tags]  lvl:regress
    Flakiness  2  dbfail  Regression test to check whether contract closure works.

regression: Contract closure - alternative flow 1
    [Tags]  lvl:regress
    Flakiness  5  appfail  Regression test to check whether contract closure works.

regression: Contract closure - alternative flow 2
    [Tags]  lvl:regress
    Flakiness  8  appfail  Regression test to check whether contract closure works.

regression: Contract closure - complex flow
    [Tags]  lvl:regress
    Flakiness  0  appfail  Regression test to check whether contract closure works.

regression: Contract closure - negative flow 1
    [Tags]  lvl:regress
    Flakiness  15  prod_bug  Regression test to check whether contract closure works.

regression: Contract closure - negative flow 2
    [Tags]  lvl:regress
    Flakiness  0  prod_bug  Regression test to check whether contract closure works.

regression: Contract closure - negative flow 3
    [Tags]  lvl:regress
    Flakiness  0  prod_bug  Regression test to check whether contract closure works.

regression: Contract closure - negative flow 4
    [Tags]  lvl:regress
    Flakiness  0  prod_bug  Regression test to check whether contract closure works.

regression: Contract closure - negative flow 5
    [Tags]  lvl:regress
    Flakiness  0  prod_bug  Regression test to check whether contract closure works.