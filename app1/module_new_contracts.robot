*** Settings ***
Resource  ../resources/flakiness.resource
Force Tags  feature:new contract

*** Test Cases ***
regression: New contract - simple flow
    [Tags]  lvl:regress  fgroup:contracts
    Flakiness  2  sys_bug  Regression test to check whether simple flow new contract openning works.

regression: New contract - alternative flow 1
    [Tags]  lvl:regress  fgroup:contracts
    Flakiness  30  prod_bug  Regression test to check whether complex flow for new contract openning works.

regression: New contract - alternative flow 2
    [Tags]  lvl:regress  fgroup:contracts
    Flakiness  30  prod_bug  Regression test to check whether complex flow for new contract openning works.

regression: New contract - alternative flow 3
    [Tags]  lvl:regress  fgroup:contracts
    Flakiness  30  auto_bug  Regression test to check whether complex flow for new contract openning works.

regression: New contract - negative flow 1
    [Tags]  lvl:regress  fgroup:contracts
    Flakiness  5  no_defect  Regression test to check whether negative flow for new contract openning works.

regression: New contract - complex flow
    [Tags]  lvl:regress  fgroup:contracts
    Flakiness  30  prod_bug  Regression test to check whether complex flow for new contract openning works.

regression: New contract - negative flow 2
    [Tags]  lvl:regress  fgroup:contracts
    Flakiness  5  auto_bug  Regression test to check whether negative flow for new contract openning works.