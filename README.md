# Report Portal Demo

## Initial setup

1. Instal venv

`python3 -m venv venv`

2. Activate it

`source ./venv/bin/activate` OR `.\venv\Scripts\Activate.ps1`

3. Instal dependencies

`pip install -r ./resources/requirements.txt`

4. Run it

`robot --listener robotframework_reportportal.listener --listener resources/scr.py -V ./resources/variables.py -v RP_LAUNCH:"full App1" ./app1/*`

## Demo: generate data before WS

_Note: Works not in PyCharm terminal. Use with powershell or Win terminal_

```
.\venv\Scripts\Activate.ps1
for ($i=1; $i -le 10; $i++) {robot --listener robotframework_reportportal.listener --listener resources/scr.py  -V ./resources/variables.py -v RP_LAUNCH:"full App1" ./app1/*}
for ($i=1; $i -le 10; $i++) {robot --listener robotframework_reportportal.listener --listener resources/scr.py  -V ./resources/variables.py -v RP_LAUNCH:"full App2" ./app2/*}
```

## Demo

```
robot --listener robotframework_reportportal.listener --listener resources/scr.py  -V ./resources/variables.py -v RP_LAUNCH:"full App1" ./app1/*
robot --listener robotframework_reportportal.listener --listener resources/scr.py  -V ./resources/variables.py -v RP_LAUNCH:"full App2" ./app2/*
```

Other examples:

```
robot --listener robotframework_reportportal.listener --listener resources/scr.py  -V ./resources/variables.py -v RP_LAUNCH:"smoke App2" ./app2/*

robot --listener robotframework_reportportal.listener -V ./resources/variables.py -v RP_LAUNCH:"mon App1" --include mon ./app1/*
robot --listener robotframework_reportportal.listener -V ./resources/variables.py -v RP_LAUNCH:"smoke App1" --include smoke ./app1/*
robot --listener robotframework_reportportal.listener -V ./resources/variables.py -v RP_LAUNCH:"full App1" ./app1/*

robot --listener robotframework_reportportal.listener -V ./resources/variables.py -v RP_LAUNCH:"mon App2" --include mon ./app2/*
robot --listener robotframework_reportportal.listener -V ./resources/variables.py -v RP_LAUNCH:"smoke App2" --include smoke ./app2/*
robot --listener robotframework_reportportal.listener -V ./resources/variables.py -v RP_LAUNCH:"full App2" ./app2/*

robot --listener robotframework_reportportal.listener -V ./resources/variables.py -v RP_LAUNCH_ATTRIBUTES:"platform:android" -v RP_LAUNCH:"full App1" ./app1/*
```

# Updating webdriver

Chrome webdriver need to be always up to date. For the demo chrome is enough. To update is this command is enough to execute: `webdrivermanager chrome`

Note: [webdrivermanager](https://pypi.org/project/webdrivermanager/) need to be set up to simply update version of drivers.
