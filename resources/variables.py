from decouple import config

URL = "https://marcel.veselka.gitlab.io/demo-cicd/"

# --listener robotframework_reportportal.listener
RP_UUID = config('RP_UUID')  # set this up in your system env.(for local run) or in CI/CD variables
RP_ENDPOINT = "http://portal.tesena.com"
RP_LAUNCH = "marcel.veselka_TEST_EXAMPLE"
RP_PROJECT = "default_personal"

# RP_REPORTING_ASYNC = True
# RP_RERUN = True
# RP_RERUN_OF = ""

# REQUIRED
# rp.endpoint = http://localhost:8080
# rp.uuid = 37b9cc48-014b-444a-8931-51767e8b6e77
# rp.launch = default_TEST_EXAMPLE
# rp.project = default_personal

# NOT REQUIRED
# rp.enable = true
# rp.description = My awesome launch
# rp.attributes = key:value; value;
# rp.convertimage = true
# rp.mode = DEFAULT
# rp.skipped.issue = true
# rp.batch.size.logs = 20
# rp.keystore.resource = <PATH_TO_YOUR_KEYSTORE>
# rp.keystore.password = <PASSWORD_OF_YOUR_KEYSTORE>