# Doc: https://github.com/reportportal/client-Python#send-attachement-screenshots

from robot.libraries.BuiltIn import BuiltIn
from robotframework_reportportal import logger

ROBOT_LISTENER_API_VERSION = 2

items = []


def end_keyword(name, attributes):
    """End a keyword."""

    if name == "SeleniumLibrary.Capture Page Screenshot":
        screenshot_file_path = BuiltIn().get_variable_value("${path}")

        with open(screenshot_file_path, "rb") as image_file:
            file_data = image_file.read()

        logger.info("Uploading screenshot",
                    attachment={"name": screenshot_file_path,
                                "data": file_data,
                                "mime": "image/png"})

