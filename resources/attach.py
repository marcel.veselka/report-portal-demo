from robotframework_reportportal import logger


def log_screens(screenshot_file_path):
    with open(screenshot_file_path, "rb") as image_file:
        file_data = image_file.read()

    logger.info("Uploading screenshot",
                attachment={"name": screenshot_file_path,
                            "data": file_data,
                            "mime": "image/png"})

# Doc: https://github.com/reportportal/client-Python#send-attachement-screenshots
